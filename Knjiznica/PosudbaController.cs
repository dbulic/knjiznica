﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Knjiznica
{
    class PosudbaController // klasa u kojoj su metode za dohvat i spremanje posudbi na API
    {
        public static async Task<PosudbaModel[]> GetPosudbeAsync()
        {
            try
            {
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["knjiznica_id"] = Form1.Knjiznica; // dohvati id knjiznice iz staticke varijable u Form1 klasi
                var data = await ApiHelper.GetDataAsync<DataCapsule<PosudbaModel>>($"borrow/?{query}");
                return data.Data;
            }
            catch
            {
                throw;
            }
        }
        public static async Task<PosudbaModel> PostPosudbaAsync(PosudbaUpload posudba)
        {
            try
            {
                Console.WriteLine(posudba.SerializeData());
                var data = await ApiHelper.PostDataAsync<PosudbaModel>("borrow", posudba);
                return data;
            }
            catch
            {
                throw;
            }
        }

        public static async Task<PosudbaModel> PostGiveBackPosudbaAsync(PosudbaModel posudba)
        {
            try
            {
                var data = await ApiHelper.PostDataAsync<PosudbaModel>("borrow/return", posudba);
                return data;
            }
            catch
            {
                throw;
            }
        }
    }
}
