﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class PretraziPosudbe : UserControl
    {
        public PretraziPosudbe()
        {
            InitializeComponent();
        }

        private async void PretraziPosudbe_VisibleChanged(object sender, EventArgs e) // kada se prikaze ovaj usercontrol
        {
            if (!DesignMode) // sprijecava crashavanje visual studia kad se otvori FORM1.cs(design)
            {
                if (Visible && !Disposing)
                {
                    try
                    {
                        // dohvacamo posudbe sa APIja i punimo tablicu
                        dataGridView1.DataSource = await PosudbaController.GetPosudbeAsync();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private async void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!DesignMode) // sprijecava crashavanje visual studia pri otvaranju Form1.cs(design)
            {
                if (e.RowIndex != -1)
                {
                    if (dataGridView1.CurrentCell != null && dataGridView1.CurrentCell.Value != null)
                    {
                        var odabrana = dataGridView1.CurrentRow.DataBoundItem as PosudbaModel;
                        DialogResult result = MessageBox.Show("Sigurno želite vratiti ovu knjigu?", "Potvrda", MessageBoxButtons.YesNo);

                        if (result == DialogResult.Yes)
                        {
                            try
                            {
                                var posudba = await PosudbaController.PostGiveBackPosudbaAsync(odabrana);
                                MessageBox.Show("Vracena posudba: " + posudba);
                            }
                            catch (Exception ex){
                                //MessageBox.Show(ex.Message);
                                Console.WriteLine(ex.Message);
                                Console.WriteLine(ex.StackTrace);
                            }

                            try
                            {
                                // dohvacamo posudbe sa APIja i punimo tablicu
                                dataGridView1.DataSource = await PosudbaController.GetPosudbeAsync();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Trim() == "")
                {
                    try
                    {
                        // dohvacamo posudbe sa APIja i punimo tablicu
                        dataGridView1.DataSource = await PosudbaController.GetPosudbeAsync();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    dataGridView1.DataSource = await SearchController.SearchPosudbeAsync(textBox1.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
