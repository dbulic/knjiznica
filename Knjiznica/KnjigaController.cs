﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class KnjigaController // klasa u kojoj su metode za dohvat i spremanje knjiga na API
    {
        public static async Task<KnjigaModel[]> GetKnjigeAsync()
        {
            try
            {
                var data = await ApiHelper.GetDataAsync<DataCapsule<KnjigaModel>>("book");
                return data.Data;
            }
            catch 
            {
                throw;
            }
        }
        public static async Task<KnjigaModel> PostKnjigaAsync(KnjigaUpload knjiga)
        {
            try
            {
                //Console.WriteLine(knjiga.SerializeData());
                var data = await ApiHelper.PostDataAsync<KnjigaModel>("book", knjiga);
                return data;
            }
            catch
            {
                throw;
            }
        }
    }
}
