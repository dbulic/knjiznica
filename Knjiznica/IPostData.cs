﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    public interface IPostData // sucelje koje moraju implementirati sve klase koje se koriste za kreiranje podataka (slanje na API)
    {
        string SerializeData();
    }
}
