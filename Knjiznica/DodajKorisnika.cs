﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class DodajKorisnika : UserControl
    {
        public DodajKorisnika()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // slanje objekta modela podataka novog kosrisnika na API, podatci se dohvacaju iz polja za unos
                var noviKorisnik = await KorisnikController.PostKorisnikAsync(new KorisnikUpload(
                    ime.Text,
                    prezime.Text,
                    adresa.Text,
                    br_mob.Text,
                    dateTimePicker1.Value
                ));

                MessageBox.Show("Dodan novi korisnik: " + noviKorisnik); // obavijesti korisnika o uspjesnosti slanja
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.Message);
                Console.WriteLine(e1.StackTrace);
            }
        }

        private void DodajKorisnika_VisibleChanged(object sender, EventArgs e) // kada se pokaze ovaj usercontrol...
        {
            if (!DesignMode) // sprijecava crashanje visual studia pri otvaranju Form1.cs(design)
            {
                if (Visible && !Disposing) // resetiraj sva polja za unos...
                {
                    ime.Text = "";
                    prezime.Text = "";
                    adresa.Text = "";
                    br_mob.Text = "";
                    dateTimePicker1.Value = DateTime.Now;
                    label7.Text = "";
                }
            }
        }
    }
}
