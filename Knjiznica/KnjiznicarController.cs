﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    public class KnjiznicarController // klasa u kojoj su metode za dohvat i spremanje knjiznicara na API
    {
        public static async Task<KnjiznicarModel[]> GetKnjiznicarAsync()
        {
            try
            {
                var data = await ApiHelper.GetDataAsync<DataCapsule<KnjiznicarModel>>("librarian");
                return data.Data;
            }
            catch 
            {
                throw;
            }
        }
    }
}
