﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class NaslovModel // klasa u kojoj je definiran model podataka naslova kojeg dohvacamo sa APIja
    {
        public string Id { get; set; }
        public string Naslov { get; set; }
        public AutorModel Author { get; set; }

        public override string ToString()
        {
            return $"{Id}";
        }
    }
}
