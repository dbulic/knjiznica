﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class DodajKnjigu : UserControl // usercontrol za dodavanje novih knjiga u sustav
    {
        public DodajKnjigu()
        {
            InitializeComponent();
        }

        private async void DodajKnjigu_VisibleChanged(object sender, EventArgs e) // kada se prikaze usercontrol...
        {
            if (!DesignMode) // sprijecava crashavanje visual studia pri otvaranju Form1.cs(design)
            {
                if(Visible && !Disposing)
                {
                    var oznake = await NaslovController.GetNaslovAsync();
                    foreach(var oznaka in oznake)
                    {
                        comboBox1.Items.Add(oznaka);
                    }
                }
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.SelectedIndex < 0)
                {
                    throw new Exception("Nije odabran naslov");
                }
                var knjiga = await KnjigaController.PostKnjigaAsync(new KnjigaUpload(
                        (comboBox1.SelectedItem as NaslovModel).Id.ToString(), 
                        Int32.Parse(Form1.Knjiznica)
                    ));
                MessageBox.Show("Dodana knjiga " + knjiga);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
