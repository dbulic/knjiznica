﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class KorisnikController // klasa u kojoj su metode za dohvat i spremanje korinika na API
    {
        public static async Task<KorisnikModel[]> GetKorisnikAsync()
        {
            try
            {
                var data = await ApiHelper.GetDataAsync<DataCapsule<KorisnikModel>>("user");
                return data.Data;
            }
            catch 
            {
                throw;
            }
        }

        public static async Task<KorisnikModel> PostKorisnikAsync(KorisnikUpload kor)
        {
            try
            {
                var data = await ApiHelper.PostDataAsync<KorisnikModel>("user", kor);
                return data;
            }
            catch
            {
                throw;
            }
        }

        public static async Task<KorisnikModel> UpdateKorisnikAsync(int id, KorisnikUpload kor)
        {
            try
            {
                var data = await ApiHelper.PostDataAsync<KorisnikModel>("user/"+id, kor); // idealno bi trebalo koristiti PUT request ali radi i ovako :)
                return data;
            }
            catch
            {
                throw;
            }
        }
    }
}
