﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    public class KnjiznicaController // klasa u kojoj su metode za dohvat i spremanje knjiznica na API
    {
        public static async Task<KnjiznicaModel[]> GetKnjiznice()
        {
            try
            {
                var data = await ApiHelper.GetDataAsync<DataCapsule<KnjiznicaModel>>("library");
                return data.Data;
            }
            catch
            {
                throw;
            }
        }
    }
}
