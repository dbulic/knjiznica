﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Knjiznica
{
    class SearchController
    {
        public static async Task<PosudbaModel[]> SearchPosudbeAsync(string req)
        {
            try
            {
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["table"] = "borrow";
                query["data"] = req;
                var data = await ApiHelper.GetDataAsync<DataCapsule<PosudbaModel>>($"search/?{query}");
                return data.Data;
            }
            catch
            {
                throw;
            }
        }

        public static async Task<KnjigaModel[]> SearchKnjigeAsync(string req)
        {
            try
            {
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["table"] = "book";
                query["data"] = req;
                var data = await ApiHelper.GetDataAsync<DataCapsule<KnjigaModel>>($"search/?{query}");
                return data.Data;
            }
            catch
            {
                throw;
            }
        }

        public static async Task<KorisnikModel[]> SearchKorisnikAsync(string req)
        {
            try
            {
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["table"] = "user";
                query["data"] = req;
                var data = await ApiHelper.GetDataAsync<DataCapsule<KorisnikModel>>($"search/?{query}");
                return data.Data;
            }
            catch
            {
                throw;
            }
        }
    }
}
