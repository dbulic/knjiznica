﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class AutorController // klasa u kojoj su metode za dohvat i spremanje autora na API
    {
        /**
         *<summary>Dohvaca autore sa APIja asinkrono</summary> 
         */
        public static async Task<AutorModel[]> GetAutoriAsync()
        {
            try
            {
                var data = await ApiHelper.GetDataAsync<DataCapsule<AutorModel>>("author");
                return data.Data;
            }
            catch
            {
                throw;
            }
        }

        /**
         *<summary>Salje post requestom autora na API asinkrono</summary> 
         */
        public static async Task<AutorModel> PostAutorAsync(AutorUpload autor)
        {
            try
            {
                //Console.WriteLine(autor.SerializeData());
                var data = await ApiHelper.PostDataAsync<AutorModel>("author", autor);
                return data;
            }
            catch
            {
                throw;
            }
        }
    }
}
