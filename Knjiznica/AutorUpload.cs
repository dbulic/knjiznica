﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class AutorUpload : IPostData // klasa u kojoj su definirani podatci za stvaranje novog autora i slanje na API
    {
        public string Ime;
        public string Prezime;

        public AutorUpload(string ime, string prezime)
        {
            Ime = ime;
            Prezime = prezime;
        }
        public string SerializeData() // vraca JSON objekt s podatcima u stringu, spreman za slanje na API
        {
            return $"{{\"ime\":\"{Ime}\",\"prezime\":\"{Prezime}\"}}";
        }
    }
}
