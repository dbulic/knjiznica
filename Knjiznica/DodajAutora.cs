﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class DodajAutora : UserControl
    {
        public DodajAutora()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var autor = await AutorController.PostAutorAsync(new AutorUpload(
                        textBox1.Text,
                        textBox2.Text
                    ));
                MessageBox.Show("Dodan novi autor: "+autor);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
