﻿namespace Knjiznica
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajNaslovToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajKnjiguToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajKorisnikaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.posudiKnjiguToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pretražiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pretražiPosudbeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pretražiKnjigeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pretražiKorisnikeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.urediKorisnika1 = new Knjiznica.UrediKorisnika();
            this.pretraziPosudbe1 = new Knjiznica.PretraziPosudbe();
            this.pretraziKorisnike1 = new Knjiznica.PretraziKorisnike();
            this.pretraziKnjige1 = new Knjiznica.PretraziKnjige();
            this.posudi1 = new Knjiznica.Posudi();
            this.dodajNaslov1 = new Knjiznica.DodajNaslov();
            this.dodajKorisnika1 = new Knjiznica.DodajKorisnika();
            this.dodajKnjigu1 = new Knjiznica.DodajKnjigu();
            this.dodajAutora1 = new Knjiznica.DodajAutora();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.posudiKnjiguToolStripMenuItem,
            this.pretražiToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1036, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem1,
            this.dodajNaslovToolStripMenuItem,
            this.dodajKnjiguToolStripMenuItem,
            this.dodajKorisnikaToolStripMenuItem});
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.dodajToolStripMenuItem.Text = "Dodaj";
            // 
            // dodajToolStripMenuItem1
            // 
            this.dodajToolStripMenuItem1.Name = "dodajToolStripMenuItem1";
            this.dodajToolStripMenuItem1.Size = new System.Drawing.Size(155, 22);
            this.dodajToolStripMenuItem1.Text = "Dodaj autora";
            this.dodajToolStripMenuItem1.Click += new System.EventHandler(this.dodajToolStripMenuItem1_Click);
            // 
            // dodajNaslovToolStripMenuItem
            // 
            this.dodajNaslovToolStripMenuItem.Name = "dodajNaslovToolStripMenuItem";
            this.dodajNaslovToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.dodajNaslovToolStripMenuItem.Text = "Dodaj naslov";
            this.dodajNaslovToolStripMenuItem.Click += new System.EventHandler(this.dodajNaslovToolStripMenuItem_Click);
            // 
            // dodajKnjiguToolStripMenuItem
            // 
            this.dodajKnjiguToolStripMenuItem.Name = "dodajKnjiguToolStripMenuItem";
            this.dodajKnjiguToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.dodajKnjiguToolStripMenuItem.Text = "Dodaj knjigu";
            this.dodajKnjiguToolStripMenuItem.Click += new System.EventHandler(this.dodajKnjiguToolStripMenuItem_Click);
            // 
            // dodajKorisnikaToolStripMenuItem
            // 
            this.dodajKorisnikaToolStripMenuItem.Name = "dodajKorisnikaToolStripMenuItem";
            this.dodajKorisnikaToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.dodajKorisnikaToolStripMenuItem.Text = "Dodaj korisnika";
            this.dodajKorisnikaToolStripMenuItem.Click += new System.EventHandler(this.dodajKorisnikaToolStripMenuItem_Click);
            // 
            // posudiKnjiguToolStripMenuItem
            // 
            this.posudiKnjiguToolStripMenuItem.Name = "posudiKnjiguToolStripMenuItem";
            this.posudiKnjiguToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.posudiKnjiguToolStripMenuItem.Text = "Posudi knjigu";
            this.posudiKnjiguToolStripMenuItem.Click += new System.EventHandler(this.posudiKnjiguToolStripMenuItem_Click);
            // 
            // pretražiToolStripMenuItem
            // 
            this.pretražiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pretražiPosudbeToolStripMenuItem,
            this.pretražiKnjigeToolStripMenuItem,
            this.pretražiKorisnikeToolStripMenuItem});
            this.pretražiToolStripMenuItem.Name = "pretražiToolStripMenuItem";
            this.pretražiToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.pretražiToolStripMenuItem.Text = "Pretraži";
            // 
            // pretražiPosudbeToolStripMenuItem
            // 
            this.pretražiPosudbeToolStripMenuItem.Name = "pretražiPosudbeToolStripMenuItem";
            this.pretražiPosudbeToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.pretražiPosudbeToolStripMenuItem.Text = "Pretraži posudbe";
            this.pretražiPosudbeToolStripMenuItem.Click += new System.EventHandler(this.pretražiPosudbeToolStripMenuItem_Click);
            // 
            // pretražiKnjigeToolStripMenuItem
            // 
            this.pretražiKnjigeToolStripMenuItem.Name = "pretražiKnjigeToolStripMenuItem";
            this.pretražiKnjigeToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.pretražiKnjigeToolStripMenuItem.Text = "Pretraži knjige";
            this.pretražiKnjigeToolStripMenuItem.Click += new System.EventHandler(this.pretražiKnjigeToolStripMenuItem_Click);
            // 
            // pretražiKorisnikeToolStripMenuItem
            // 
            this.pretražiKorisnikeToolStripMenuItem.Name = "pretražiKorisnikeToolStripMenuItem";
            this.pretražiKorisnikeToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.pretražiKorisnikeToolStripMenuItem.Text = "Pretraži korisnike";
            this.pretražiKorisnikeToolStripMenuItem.Click += new System.EventHandler(this.pretražiKorisnikeToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(558, 343);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Prijava";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(475, 288);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(158, 21);
            this.comboBox2.TabIndex = 13;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(475, 243);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(158, 21);
            this.comboBox1.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(372, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Knjižnica:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(372, 246);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Knjižničar:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 31);
            this.label1.TabIndex = 9;
            this.label1.Text = "Prijava";
            // 
            // urediKorisnika1
            // 
            this.urediKorisnika1.Korisnik = null;
            this.urediKorisnika1.Location = new System.Drawing.Point(0, 27);
            this.urediKorisnika1.Name = "urediKorisnika1";
            this.urediKorisnika1.Size = new System.Drawing.Size(1036, 569);
            this.urediKorisnika1.TabIndex = 15;
            // 
            // pretraziPosudbe1
            // 
            this.pretraziPosudbe1.Location = new System.Drawing.Point(0, 27);
            this.pretraziPosudbe1.Name = "pretraziPosudbe1";
            this.pretraziPosudbe1.Size = new System.Drawing.Size(1099, 590);
            this.pretraziPosudbe1.TabIndex = 8;
            // 
            // pretraziKorisnike1
            // 
            this.pretraziKorisnike1.Location = new System.Drawing.Point(0, 27);
            this.pretraziKorisnike1.Name = "pretraziKorisnike1";
            this.pretraziKorisnike1.Odabran = null;
            this.pretraziKorisnike1.Size = new System.Drawing.Size(1099, 590);
            this.pretraziKorisnike1.TabIndex = 7;
            // 
            // pretraziKnjige1
            // 
            this.pretraziKnjige1.Location = new System.Drawing.Point(0, 27);
            this.pretraziKnjige1.Name = "pretraziKnjige1";
            this.pretraziKnjige1.Size = new System.Drawing.Size(1099, 590);
            this.pretraziKnjige1.TabIndex = 6;
            // 
            // posudi1
            // 
            this.posudi1.Location = new System.Drawing.Point(0, 27);
            this.posudi1.Name = "posudi1";
            this.posudi1.Size = new System.Drawing.Size(1099, 590);
            this.posudi1.TabIndex = 5;
            // 
            // dodajNaslov1
            // 
            this.dodajNaslov1.Location = new System.Drawing.Point(0, 27);
            this.dodajNaslov1.Name = "dodajNaslov1";
            this.dodajNaslov1.Size = new System.Drawing.Size(1099, 590);
            this.dodajNaslov1.TabIndex = 4;
            // 
            // dodajKorisnika1
            // 
            this.dodajKorisnika1.Location = new System.Drawing.Point(0, 27);
            this.dodajKorisnika1.Name = "dodajKorisnika1";
            this.dodajKorisnika1.Size = new System.Drawing.Size(1099, 590);
            this.dodajKorisnika1.TabIndex = 3;
            // 
            // dodajKnjigu1
            // 
            this.dodajKnjigu1.Location = new System.Drawing.Point(0, 27);
            this.dodajKnjigu1.Name = "dodajKnjigu1";
            this.dodajKnjigu1.Size = new System.Drawing.Size(1099, 590);
            this.dodajKnjigu1.TabIndex = 2;
            // 
            // dodajAutora1
            // 
            this.dodajAutora1.Location = new System.Drawing.Point(0, 27);
            this.dodajAutora1.Name = "dodajAutora1";
            this.dodajAutora1.Size = new System.Drawing.Size(1099, 590);
            this.dodajAutora1.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 594);
            this.Controls.Add(this.urediKorisnika1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pretraziPosudbe1);
            this.Controls.Add(this.pretraziKorisnike1);
            this.Controls.Add(this.pretraziKnjige1);
            this.Controls.Add(this.posudi1);
            this.Controls.Add(this.dodajNaslov1);
            this.Controls.Add(this.dodajKorisnika1);
            this.Controls.Add(this.dodajKnjigu1);
            this.Controls.Add(this.dodajAutora1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(1052, 633);
            this.MinimumSize = new System.Drawing.Size(1052, 633);
            this.Name = "Form1";
            this.Text = "Knjiznica";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dodajNaslovToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajKnjiguToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajKorisnikaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem posudiKnjiguToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pretražiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pretražiPosudbeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pretražiKnjigeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pretražiKorisnikeToolStripMenuItem;
        private DodajAutora dodajAutora1;
        private DodajKnjigu dodajKnjigu1;
        private DodajKorisnika dodajKorisnika1;
        private DodajNaslov dodajNaslov1;
        private Posudi posudi1;
        private PretraziKnjige pretraziKnjige1;
        private PretraziKorisnike pretraziKorisnike1;
        private PretraziPosudbe pretraziPosudbe1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private UrediKorisnika urediKorisnika1;
    }
}

