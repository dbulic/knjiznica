﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    public class KorisnikModel // klasa u kojoj je definiran model podataka korisnika kojeg dohvacamo sa APIja
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Adresa { get; set; }
        public string Br_mob { get; set; }
        public DateTime Clan_do { get; set; }

        public override string ToString()
        {
            return $"{Ime} {Prezime}";
        }
    }
}
