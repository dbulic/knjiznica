﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class NaslovController // klasa u kojoj su metode za dohvat i spremanje naslova na API
    {
        public static async Task<NaslovModel[]> GetNaslovAsync()
        {
            try
            {
                var data = await ApiHelper.GetDataAsync<DataCapsule<NaslovModel>>("title");
                return data.Data;
            }
            catch
            {
                throw;
            }
        }
        public static async Task<NaslovModel> PostNaslovAsync(NaslovUpload naslov)
        {
            try
            {
                //Console.WriteLine(naslov.SerializeData());
                var data = await ApiHelper.PostDataAsync<NaslovModel>("title", naslov);
                return data;
            }
            catch
            {
                throw;
            }
        }
    }
}
