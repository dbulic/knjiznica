﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class PosudbaModel : IPostData // klasa u kojoj je definiran model podataka posudbe kojeg dohvacamo sa APIja
    {
        public int Id { get; set; }
        public KnjigaModel Book { get; set; }
        public KorisnikModel User { get; set; }
        public string Uzeo { get; set; }
        public string Vratio { get; set; }
        public KnjiznicarModel Librarian { get; set; }

        public string SerializeData()
        {
            return $"{{\"id\":{ Id }}}";
        }

        public override string ToString()
        {
            return Id.ToString();
        }
    }
}
