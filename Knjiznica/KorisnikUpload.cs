﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class KorisnikUpload : IPostData // klasa u kojoj su definirani podatci za stvaranje novog korisnika i slanje na API
    {
        public string Ime;
        public string Prezime;
        public string Adresa;
        public string Br_mob;
        public DateTime Clan_do;

        public KorisnikUpload(string ime, string prezime, string adresa, string br_mob, DateTime clan_do)
        {
            Ime = ime;
            Prezime = prezime;
            Adresa = adresa;
            Br_mob = br_mob;
            Clan_do = clan_do;
        }
        public string SerializeData() // vraca JSON objekt s podatcima u stringu, spreman za slanje na API
        {
            return $"{{ \"ime\":\"{Ime}\",\"prezime\":\"{Prezime}\",\"adresa\":\"{Adresa}\",\"br_mob\":\"{Br_mob}\",\"clan_do\":\"{Clan_do.ToString("yyyy-MM-dd HH:mm:ss")}\"}}";
        }
    }
}
