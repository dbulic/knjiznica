﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    class PosudbaUpload : IPostData // klasa u kojoj su definirani podatci za stvaranje nove posudbe i slanje na API
    {
        public int BookId;
        public int UserId;
        public DateTime Uzeo;
        public int LibrarianId;

        public PosudbaUpload(int bookId, int userId, DateTime uzeo, int librarianId)
        {
            BookId = bookId;
            UserId = userId;
            Uzeo = uzeo;
            LibrarianId = librarianId;
        }

        public string SerializeData() // vraca JSON objekt s podatcima u stringu, spreman za slanje na API
        {
            return $"{{\"book_id\":{BookId},\"user_id\":{UserId},\"uzeo\":\"{Uzeo.ToString("yyyy-MM-dd HH:mm:ss")}\",\"librarian_id\":{LibrarianId}}}";
        }
    }
}
