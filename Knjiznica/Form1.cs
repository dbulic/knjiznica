﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class Form1 : Form
    {
        // staticne varijable u kojima se spremaju koji je knjiznicar i koja je knjiznica vezana uz instancu programa
        public static string Knjiznica;    
        public static string Knjiznicar;


        public Form1()
        {
            InitializeComponent();
            ApiHelper.InitializeClient("https://test.dbulic.com/api/"); // inicijaliziraj klijenta za komunikaciju s APIjem (postavlja se bazni URL)
        }

        private void sakrijSveUserControle() // sakriva sve UserControl elemente na form1
        {
            urediKorisnika1.Hide();
            dodajAutora1.Hide();
            dodajNaslov1.Hide();
            dodajKnjigu1.Hide();
            dodajKorisnika1.Hide();
            posudi1.Hide();
            pretraziKnjige1.Hide();
            pretraziPosudbe1.Hide();
            pretraziKorisnike1.Hide();
        }

        private void dodajToolStripMenuItem1_Click(object sender, EventArgs e) // pokazi UserControl za dodavanje autora 
        {
            sakrijSveUserControle();
            dodajAutora1.Show();
            dodajAutora1.BringToFront();
        }

        private async void Form1_Load(object sender, EventArgs e) // kad je forma ucitana...
        {
            sakrijSveUserControle();
            menuStrip1.Hide(); // sakrij meni dok ne prode prijava knjiznicara
            pretraziKorisnike1.KorisnikKliknut += odaberiKorisnika;

            try
            {
                var data = await KnjiznicaController.GetKnjiznice(); // dohvati sve knjiznice
                var data2 = await KnjiznicarController.GetKnjiznicarAsync(); // dohvati sve knjiznicare

                foreach (var knjiznica in data) // popuni padajuci izbornik knjiznicama
                {
                    comboBox2.Items.Add(knjiznica);
                }

                foreach (var knjiznicar in data2) // popuni padajuci izbornik knjiznicarima
                {
                    comboBox1.Items.Add(knjiznicar);
                }

                // id knjiznice se moze dohvatit ovako:  (comboBox2.SelectedItem as KnjiznicaModel).Id.ToString()
                // id knjiznicara se moze dohvatit ovako:  (comboBox1.SelectedItem as KnjiznicarModel).Id.ToString()
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.StackTrace);
                Console.WriteLine(e1.Message);
            }
        }

        private void odaberiKorisnika(object sender) // metoda oja se izvodi kada se triggera custom event za odabir korisnika iz tablice
        {
            urediKorisnika1.Korisnik = pretraziKorisnike1.Odabran; // dohvati odabranog korisnika i spremi ga u varijablu usercontrola za uredivanje korisnika

            // prikazi usercontrol za uredibanje korisnika
            pretraziKorisnike1.Hide();
            urediKorisnika1.Show();
            urediKorisnika1.BringToFront();
        }

        private void dodajNaslovToolStripMenuItem_Click(object sender, EventArgs e) // pokazi UserControl za dodavanje naslova 
        {
            sakrijSveUserControle();
            dodajNaslov1.Show();
            dodajNaslov1.BringToFront();
        }

        private void dodajKnjiguToolStripMenuItem_Click(object sender, EventArgs e) // pokazi UserControl za dodavanje knjiga 
        {
            sakrijSveUserControle();
            dodajKnjigu1.Show();
            dodajKnjigu1.BringToFront();
        }

        private void dodajKorisnikaToolStripMenuItem_Click(object sender, EventArgs e) // pokazi UserControl za dodavanje korisnika 
        {
            sakrijSveUserControle();
            dodajKorisnika1.Show();
            dodajKorisnika1.BringToFront();
        }

        private void posudiKnjiguToolStripMenuItem_Click(object sender, EventArgs e) // pokazi UserControl za dodavanje posudbi 
        {
            sakrijSveUserControle();
            posudi1.Show();
            posudi1.BringToFront();
        }

        private void pretražiPosudbeToolStripMenuItem_Click(object sender, EventArgs e) // pokazi UserControl za pretrazivanje posudbi
        {
            sakrijSveUserControle();
            pretraziPosudbe1.Show();
            pretraziPosudbe1.BringToFront();
        }

        private void pretražiKnjigeToolStripMenuItem_Click(object sender, EventArgs e) // pokazi UserControl za pretrazivanje knjiga
        {
            sakrijSveUserControle();
            pretraziKnjige1.Show();
            pretraziKnjige1.BringToFront();
            
        }

        private void pretražiKorisnikeToolStripMenuItem_Click(object sender, EventArgs e) // pokazi UserControl za pretrazivanje korisnika
        {
            sakrijSveUserControle();
            pretraziKorisnike1.Show();
            pretraziKorisnike1.BringToFront();
        }

        private void button1_Click(object sender, EventArgs e) // prijava u sustav...
        {

            if(comboBox1.SelectedItem != null && comboBox2.SelectedItem != null) // provjera je li itijedan padajuci izbornik prazan
            {
                Knjiznica = (comboBox2.SelectedItem as KnjiznicaModel).Id.ToString(); // spremi knjiznicu id padajuceg izbornika
                Knjiznicar = (comboBox1.SelectedItem as KnjiznicarModel).Id.ToString(); // spremi knjiznicara iz padajuceg izbornika

                // sakrij sve elemente prijave
                comboBox1.Hide();
                comboBox2.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();

                // pokazi tablicu sa posudbama i gornji izbornik
                pretraziPosudbe1.Show();
                pretraziPosudbe1.BringToFront();
                menuStrip1.Show();
            }
            else // u slucaju da jest pokazi poruku greske
            {
                MessageBox.Show("Molimo odaberite knjižničara i knjižnicu!","Greška", MessageBoxButtons.OK);
            }
        }
    }
}
