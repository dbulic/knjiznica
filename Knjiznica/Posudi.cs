﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class Posudi : UserControl
    {
        public Posudi()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var posudba = await PosudbaController.PostPosudbaAsync(new PosudbaUpload(
                        Int32.Parse(textBox1.Text),
                        Int32.Parse(textBox2.Text),
                        dateTimePicker1.Value,
                        Int32.Parse(Form1.Knjiznicar)
                    ));
                MessageBox.Show("Id kreirane posudbe: " + posudba);
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
