﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class PretraziKnjige : UserControl
    {
        public PretraziKnjige()
        {
            InitializeComponent();
        }

        private async void PretraziKnjige_VisibleChanged(object sender, EventArgs e) // kada se prikaze ovaj usercontrol
        {
            if (!DesignMode) // sprijecava crashavanje visual studia kad se otvori FORM1.cs(design)
            {
                if (Visible && !Disposing)
                {
                    // dohvati sve knjige sa APIja i popuni tablicu
                    dataGridView1.DataSource = await KnjigaController.GetKnjigeAsync();
                }
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Trim() == "")
                {
                    try
                    {
                        // dohvacamo posudbe sa APIja i punimo tablicu
                        dataGridView1.DataSource = await KnjigaController.GetKnjigeAsync();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    dataGridView1.DataSource = await SearchController.SearchKnjigeAsync(textBox1.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
