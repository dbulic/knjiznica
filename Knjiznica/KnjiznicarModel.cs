﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiznica
{
    public class KnjiznicarModel // klasa u kojoj je definiran model podataka knjiznicara kojeg dohvacamo sa APIja
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }

        public override string ToString()
        {
            return $"{ Ime } { Prezime }";
        }

    }
}
