﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Knjiznica
{
    public static class ApiHelper // helper klasa za komunikaciju sa APIjem
    {
        public static HttpClient ApiClient { get; set; } // staticka varijabla koja sprema jednog jedinog klijenta koji vrijedi za cijeli program

        /**
         * <summary>Initialises HttpClient if doesn't exists</summary>
         * <param name="url">Base API url</param>
         */
        public static void InitializeClient(string url) // inicijalizacija klijenta za komunikaciju s APIjem
        {
            if(ApiClient == null) // ako vec postoji klijent ne radi nista a ako ne postoji stvori ga...
            {
                ApiClient = new HttpClient();
                ApiClient.BaseAddress = new Uri(url); // postavljamo bazni URL APIja predan pri inicijalizaciji
                ApiClient.DefaultRequestHeaders.Accept.Clear();
                ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); // postavljamo headere da prihvacamo samo JSON odgovore sa APIja
            }
        }

        /**
         * <summary>Fetches data from API asynchronously</summary>
         * <param name="url">API url extention (after base url)</param>
         * <returns>Object type provided as generic type argument</returns>
         */
        public static async Task<T> GetDataAsync<T>(string url) // dohvaca podatke sa APIja, potrebno je predati adresu resursa (endpoint) koji se nadovezuje na bazni URL
        {
            using(HttpResponseMessage res = await ApiClient.GetAsync(url))
            {
                if (res.IsSuccessStatusCode)
                {
                    T data = await res.Content.ReadAsAsync<T>();
                    return data;
                }
                else
                {
                    throw new Exception(res.ReasonPhrase);
                }
            }
        }

        /**
         * <summary>Posts data to API asynchronously and returns it</summary>
         * <param name="url">API url extension (after base url)</param>
         * <param name="obj">Object that will be posted to API (must implement IPostData)</param>
         * <returns>Object type provided as generic type argument</returns>
         */
        public static async Task<T> PostDataAsync<T>(string url, IPostData obj)  // salje objekt koji implementira sucelje IPostData (obj) na API, potrebno je predati adresu resursa (endpoint) koji se nadovezuje na bazni URL
        {
            using (HttpContent content = new StringContent(obj.SerializeData(), Encoding.UTF8, "application/json"))
            {
                using (HttpResponseMessage res = await ApiClient.PostAsync(url, content))
                {
                    if (res.StatusCode == System.Net.HttpStatusCode.Created || res.StatusCode == System.Net.HttpStatusCode.OK) // ako je response status 201 (za post) ili 200 (za put)...
                    {
                        T data = await res.Content.ReadAsAsync<T>();
                        return data;
                    }
                    else
                    {
                        throw new Exception(res.ReasonPhrase);
                    }
                }
            }
        }
    }
}
