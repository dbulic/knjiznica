﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class UrediKorisnika : UserControl
    {
        public KorisnikModel Korisnik { get; set; }
        public UrediKorisnika()
        {
            InitializeComponent();
        }

        private void UrediKorisnika_VisibleChanged(object sender, EventArgs e) // kada se prikaze usercontrol...
        {
            if (!DesignMode) // sprijecava crashavanje visual studia kad se otvori Form1.cs(design)... premda je ovdje viska jer ne radimo API call u ovom bloku
            {
                if(Visible && !Disposing)
                {
                    if(Korisnik != null)
                    {
                        // popuni podatke dobivene iz pretraziKorisnike usercontrola nakon triggeranja custom eventa za odabir korisnika
                        textBox1.Text = Korisnik.Ime;
                        textBox2.Text = Korisnik.Prezime;
                        textBox3.Text = Korisnik.Adresa;
                        textBox4.Text = Korisnik.Br_mob;
                        dateTimePicker1.Value = Korisnik.Clan_do;

                    }
                }
            }
        }

        private async void button1_Click(object sender, EventArgs e) // spremanje izmjene podataka o korisniku
        {
            try
            {
                // stvori novi objekt modela podataka za upload na API od podataka iz polja za unos te posalji request... odgovor spremi u var uredeni
                var uredeni = await KorisnikController.UpdateKorisnikAsync(Korisnik.Id, new KorisnikUpload(
                    textBox1.Text,
                    textBox2.Text,
                    textBox3.Text,
                    textBox4.Text,
                    dateTimePicker1.Value
                    ));
                MessageBox.Show("Uspješno uređen korisnik " + uredeni); // obavijesti korisnika o uspjesnosti promjene
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
