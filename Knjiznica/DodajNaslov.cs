﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knjiznica
{
    public partial class DodajNaslov : UserControl
    {
        public DodajNaslov()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e) // spremanje novog naslova
        {
            try
            {
                // dohvati podatke i spremi u novi objekt modela za naslov te posalji na API
                if(comboBox1.SelectedIndex < 0) {
                    throw new Exception("Nije odabran autor");
                }
                var noviNaslov = await NaslovController.PostNaslovAsync(new NaslovUpload(
                        id.Text, 
                        naslov.Text, 
                        (comboBox1.SelectedItem as AutorModel).Id
                    ));
                MessageBox.Show("Uspješno dodan novi naslov: " + noviNaslov); // obavijesti korisnika o uspjesnosti slanja
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void DodajNaslov_VisibleChanged(object sender, EventArgs e) // kada se prikaze ovaj usercontrol
        {
            if (!DesignMode) // sprijecava crashanje visual studia pri otvaranju Form1.cs(design)
            {
                if(Visible && !Disposing)
                {
                    var autori = await AutorController.GetAutoriAsync(); // dohvati sve autore sa APIja

                    // popuni padajuci izbornik s autorima
                    foreach (var autor in autori)
                    {
                        comboBox1.Items.Add(autor);
                    }
                }
            }
        }
    }
}
